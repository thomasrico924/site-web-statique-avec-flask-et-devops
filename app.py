from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/accueil')
def accueil():
    return render_template('accueil.html')

@app.route('/a_propos')
def a_propos():
    return render_template('a_propos.html')

@app.route('/contact')
def contact():
    return render_template('contact.html')

if __name__ == '__main__':
    app.run(host='192.168.188.147', port=8000)
