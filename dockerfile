FROM hajdaini/flask:random

COPY . /app
WORKDIR /app

CMD ["python", "app.py"]
